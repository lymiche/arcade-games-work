import arcade

arcade.open_window(600, 600, "Space Tardigrade")
arcade.set_background_color(arcade.csscolor.NAVY)
arcade.start_render()

#drawing rectangle bottom half
arcade.draw_lrtb_rectangle_filled(0, 599, 200, 0, arcade.csscolor.DARK_GREY)

#moon craters
arcade.draw_ellipse_filled(100, 10, 250, 170, arcade.csscolor.GREY)
arcade.draw_ellipse_filled(500, 100, 200, 120, arcade.csscolor.GREY)
arcade.draw_circle_filled(70, 150, 40, arcade.csscolor.GREY)
arcade.draw_circle_filled(350, 175, 70, arcade.csscolor.GREY)

#drawing a tardigrade body
arcade.draw_circle_filled(170, 225, 45, arcade.csscolor.LIGHT_PINK)
arcade.draw_ellipse_filled(220, 225, 110, 125, arcade.csscolor.LIGHT_PINK)
arcade.draw_ellipse_filled(275, 235, 110, 150, arcade.csscolor.LIGHT_PINK)
arcade.draw_ellipse_filled(330, 235, 110, 150, arcade.csscolor.LIGHT_PINK)
arcade.draw_ellipse_filled(385, 225, 100, 125, arcade.csscolor.LIGHT_PINK)

# front legs
arcade.draw_ellipse_filled(205, 160, 25, 40, arcade.csscolor.LIGHT_PINK)
arcade.draw_ellipse_filled(265, 160, 25, 40, arcade.csscolor.LIGHT_PINK)
arcade.draw_ellipse_filled(320, 160, 25, 40, arcade.csscolor.LIGHT_PINK)
arcade.draw_ellipse_filled(375, 160, 25, 40, arcade.csscolor.LIGHT_PINK)

# Back legs
arcade.draw_ellipse_filled(230, 175, 20, 40, arcade.csscolor.LIGHT_PINK)
arcade.draw_ellipse_filled(290, 175, 20, 40, arcade.csscolor.LIGHT_PINK)
arcade.draw_ellipse_filled(345, 175, 20, 40, arcade.csscolor.LIGHT_PINK)
arcade.draw_ellipse_filled(400, 175, 20, 40, arcade.csscolor.LIGHT_PINK)

#face
arcade.draw_circle_filled(140, 230, 3, arcade.csscolor.BLACK)
arcade.draw_circle_filled(165, 230, 3, arcade.csscolor.BLACK)
arcade.draw_arc_filled(155, 210, 30, 20, arcade.csscolor.BLACK, 180, 360)

#speech bubble
arcade.draw_ellipse_filled(225, 375, 300, 100, arcade.csscolor.WHITE)
arcade.draw_triangle_filled(150, 275, 140, 350, 200, 350, arcade.csscolor.WHITE)
arcade.draw_text("LIVE TINY, DIE NEVER!",
                 110, 365,
                 arcade.color.BLACK, 16)

#finish drawing
arcade.finish_render()

#keep window until someone closes
arcade.run()