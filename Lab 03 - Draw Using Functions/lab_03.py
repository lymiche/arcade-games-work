import arcade

SCREEN_WIDTH = 800
SCREEN_HEIGHT = 600

def draw_moon():
    # ground
    arcade.draw_lrtb_rectangle_filled(0, 800, 200, 0, arcade.color.DARK_GRAY)

    # moon craters
    arcade.draw_ellipse_filled(100, 10, 250, 170, arcade.color.OLD_SILVER)
    arcade.draw_ellipse_filled(500, 100, 200, 120, arcade.color.OLD_SILVER)
    arcade.draw_ellipse_filled(70, 150, 45, 50, arcade.color.OLD_SILVER)
    arcade.draw_ellipse_filled(350, 175, 70, 50, arcade.color.OLD_SILVER)
    arcade.draw_ellipse_filled(700, 10, 250, 120, arcade.color.OLD_SILVER)
    arcade.draw_ellipse_filled(700, 150, 100, 50, arcade.color.OLD_SILVER)

def draw_tardigrade(x, y):
    # tardigrade body
    arcade.draw_circle_filled(170 + x, 225 + y, 35, arcade.color.CAMEO_PINK)
    arcade.draw_ellipse_filled(210 + x, 230 + y, 80, 110, arcade.color.CAMEO_PINK)
    arcade.draw_ellipse_filled(250 + x, 235 + y, 80, 120, arcade.color.CAMEO_PINK)
    arcade.draw_ellipse_filled(300 + x, 235 + y, 80, 120, arcade.color.CAMEO_PINK)
    arcade.draw_ellipse_filled(340 + x, 225 + y, 70, 95, arcade.color.CAMEO_PINK)

    # front legs
    arcade.draw_ellipse_filled(205 + x, 175 + y, 15, 25, arcade.color.CAMEO_PINK)
    arcade.draw_ellipse_filled(245 + x, 175 + y, 15, 25, arcade.color.CAMEO_PINK)
    arcade.draw_ellipse_filled(295 + x, 175 + y, 15, 25, arcade.color.CAMEO_PINK)
    arcade.draw_ellipse_filled(335 + x, 175 + y, 15, 25, arcade.color.CAMEO_PINK)

    # Back legs
    arcade.draw_ellipse_filled(225 + x, 180 + y, 10, 15, arcade.color.CAMEO_PINK)
    arcade.draw_ellipse_filled(265 + x, 180 + y, 10, 15, arcade.color.CAMEO_PINK)
    arcade.draw_ellipse_filled(315 + x, 180 + y, 10, 15, arcade.color.CAMEO_PINK)
    arcade.draw_ellipse_filled(355 + x, 180 + y, 10, 15, arcade.color.CAMEO_PINK)

    # face
    arcade.draw_circle_filled(150 + x, 230 + y, 2, arcade.csscolor.BLACK)
    arcade.draw_circle_filled(170 + x, 230 + y, 2, arcade.csscolor.BLACK)
    arcade.draw_arc_filled(160 + x, 210 + y, 15, 10, arcade.csscolor.BLACK, 180, 360)

def on_draw(deltra_time):
    arcade.start_render()

    draw_moon()
    draw_tardigrade(on_draw.tardigrade_x1, 0)
    draw_tardigrade(on_draw.tardigrade_x2, -100)
    draw_tardigrade(on_draw.tardigrade_x3, -150)

    #right is pos and left is neg
    on_draw.tardigrade_x1 += -1
    on_draw.tardigrade_x2 += -2
    on_draw.tardigrade_x3 += -4

# where it will start
on_draw.tardigrade_x1 = 200
on_draw.tardigrade_x2 = -100
on_draw.tardigrade_x3 = 400

def main():
    arcade.open_window(SCREEN_WIDTH, SCREEN_HEIGHT, "Space tardigrade")
    arcade.set_background_color(arcade.color.CATALINA_BLUE)

    #calling function every 60th sec
    arcade.schedule(on_draw, 1/60)
    arcade.run()

main()